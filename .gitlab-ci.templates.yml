# template for setting up the communication via ssh and for using environment configuration from another repo
.setup_ssh_and_environment_configs:
  variables:
    GIT_ENV_CONFIG_REPO: 'https://$DEPLOY_TOKEN_ENV_CONFIG_REPO_USERNAME:$DEPLOY_TOKEN_ENV_CONFIG_REPO_PASSWORD@gitlab.com/dBPMS-PROCEED/environment-configurations.git'
    LOCAL_PATH: './configs/'
    SSH_DIRECTORY: './ssh'
    SSH_PATH: './ssh/ssh_key'
  before_script:
    # setup reuse of the same ssh key to prevent creation of a new key for every request
    - mkdir ssh
    - echo "${!PRIVATE_KEY_FILE_VARIABLE}" | tr -d '\r' > "${SSH_PATH}"
    - chmod 600 "${SSH_PATH}"
    # get environment configs
    - git clone $GIT_ENV_CONFIG_REPO $LOCAL_PATH
    - '[[ ! -z ${DEPLOYMENT_ENVIRONMENT+z} ]] && scp -o StrictHostKeyChecking=no -i "${SSH_PATH}" ${LOCAL_PATH}${DEPLOYMENT_ENVIRONMENT}/* ${!DEPLOYMENT_ENVIRONMENT_USER_VARIABLE}@$DEPLOYMENT_ENVIRONMENT_IP:$DEPLOYMENT_ENVIRONMENT_MS_DIRECTORIES_PATH/Config'
  after_script:
    - rm -rf $SSH_DIRECTORY
    - rm -rf $LOCAL_PATH

.google_compute_engine_variables:
  variables:
    PRIVATE_KEY_FILE_VARIABLE: GCLOUD_PROCEED_COMPUTE_ENGINE_PRIVATE_SSH_KEY_FILE
    DEPLOYMENT_ENVIRONMENT_USER_VARIABLE: GCLOUD_PROCEED_SERVICE_ACCOUNT_USER_NAME
    DEPLOYMENT_ENVIRONMENT_MS_DIRECTORIES_PATH: '/var/PROCEED/ms-server'
    DEPLOYMENT_ENVIRONMENT_ENGINE_DIRECTORIES_PATH: '/var/PROCEED/engine'
    DEPLOYMENT_ENVIRONMENT_LOG_DRIVER: gcplogs

.staging_variables:
  variables:
    PRIVATE_KEY_FILE_VARIABLE: STAGING_SERVER_PRIVATE_KEY_SSH_FOR_CI
    DEPLOYMENT_ENVIRONMENT_USER_VARIABLE: STAGING_SERVER_SERVICE_ACCOUNT_USER_NAME
    DEPLOYMENT_ENVIRONMENT_MS_DIRECTORIES_PATH: '/var/PROCEED/ms-server'
    DEPLOYMENT_ENVIRONMENT_ENGINE_DIRECTORIES_PATH: '/var/PROCEED/engine'
    DEPLOYMENT_ENVIRONMENT_LOG_DRIVER: local

.deploy_docker_compose_file:
  extends: .setup_ssh_and_environment_configs
  variables:
    DOCKER_COMPOSE_REPO_PATH: docker-compose.yml
    OPA_CONFIG_REPO_PATH: src/management-system/opa/config.yaml
  script:
    # clean up data from the old docker compose file on the compute engine if there is any
    - ssh -o StrictHostKeyChecking=no -i "${SSH_PATH}" ${!DEPLOYMENT_ENVIRONMENT_USER_VARIABLE}@$DEPLOYMENT_ENVIRONMENT_IP "sudo docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v "\$PWD:\$PWD" -w="\$PWD" docker/compose:1.29.0 down --rmi all || true"
    # copy opa config to compute engine
    - scp -i "${SSH_PATH}" $OPA_CONFIG_REPO_PATH ${!DEPLOYMENT_ENVIRONMENT_USER_VARIABLE}@$DEPLOYMENT_ENVIRONMENT_IP:~/opa/config.yaml
    # push new docker compose file to the compute engine
    - scp -i "${SSH_PATH}" $DOCKER_COMPOSE_REPO_PATH ${!DEPLOYMENT_ENVIRONMENT_USER_VARIABLE}@$DEPLOYMENT_ENVIRONMENT_IP:~
    # start MS services and the engine (escaped $ to prevent gitlab CI from trying to interpret PWD which can only be determined when the command is run in the compute engine)
    # TODO: restart when the compute engine restarts
    - ssh -i "${SSH_PATH}" ${!DEPLOYMENT_ENVIRONMENT_USER_VARIABLE}@$DEPLOYMENT_ENVIRONMENT_IP "sudo docker run --rm -e MS_BIND_MOUNT_DIRECTORY="$DEPLOYMENT_ENVIRONMENT_MS_DIRECTORIES_PATH" -e ENGINE_BIND_MOUNT_DIRECTORY="$DEPLOYMENT_ENVIRONMENT_ENGINE_DIRECTORIES_PATH" -e MS_LOG_DRIVER="$DEPLOYMENT_ENVIRONMENT_LOG_DRIVER" -e ENGINE_LOG_DRIVER="$DEPLOYMENT_ENVIRONMENT_LOG_DRIVER" -e MS_HTTPS_PORT="443" -e MS_TAG="$MS_IMAGE_TAG" -e ENGINE_TAG="$ENGINE_IMAGE_TAG" -v /var/run/docker.sock:/var/run/docker.sock -v "\$PWD:\$PWD" -w="\$PWD" docker/compose:1.29.0 up -d --remove-orphans"

.update_and_restart_docker_image:
  extends: .setup_ssh_and_environment_configs
  script:
    # get the new version from dockerhub
    - ssh -o StrictHostKeyChecking=no -i "${SSH_PATH}" ${!DEPLOYMENT_ENVIRONMENT_USER_VARIABLE}@$DEPLOYMENT_ENVIRONMENT_IP "sudo docker run --rm -e MS_TAG="$MS_IMAGE_TAG" -e ENGINE_TAG="$ENGINE_IMAGE_TAG" -v /var/run/docker.sock:/var/run/docker.sock -v "\$PWD:\$PWD" -w="\$PWD" docker/compose:1.29.0 pull $DOCKER_COMPOSE_SERVICE_NAME"
    # restart the engine container with the new image
    - ssh -i "${SSH_PATH}" ${!DEPLOYMENT_ENVIRONMENT_USER_VARIABLE}@$DEPLOYMENT_ENVIRONMENT_IP "sudo docker run --rm -e MS_BIND_MOUNT_DIRECTORY="$DEPLOYMENT_ENVIRONMENT_MS_DIRECTORIES_PATH" -e ENGINE_BIND_MOUNT_DIRECTORY="$DEPLOYMENT_ENVIRONMENT_ENGINE_DIRECTORIES_PATH" -e MS_LOG_DRIVER="$DEPLOYMENT_ENVIRONMENT_LOG_DRIVER" -e ENGINE_LOG_DRIVER="$DEPLOYMENT_ENVIRONMENT_LOG_DRIVER" -e MS_HTTPS_PORT="443" -e MS_TAG="$MS_IMAGE_TAG" -e ENGINE_TAG="$ENGINE_IMAGE_TAG" -v /var/run/docker.sock:/var/run/docker.sock -v "\$PWD:\$PWD" -w="\$PWD" docker/compose:1.29.0 up -d --remove-orphans $DOCKER_COMPOSE_SERVICE_NAME"
    # remove unused images
    - ssh -i "${SSH_PATH}" ${!DEPLOYMENT_ENVIRONMENT_USER_VARIABLE}@$DEPLOYMENT_ENVIRONMENT_IP "sudo docker image prune -f -a"

.update_and_restart_docker_image_on_staging:
  extends:
    - .update_and_restart_docker_image
    - .staging_variables

.update_and_restart_docker_image_on_google_compute_engine:
  extends:
    - .update_and_restart_docker_image
    - .google_compute_engine_variables
